# Guide de programmation LML

`LML` est un langage de codage des chansons pouvant être joué dans le *Play Along*
mode. LML a été conçu pour être facile à écrire à la main. LML est au début
développement, mais tout code que vous écrivez aujourd’hui sera compatible à l’avenir. Comme neuf
la fonctionnalité est ajoutée au mode *Play Along*, LML obtiendra une nouvelle syntaxe.

Chaque chanson sur cette application est écrite en LML. Cliquez sur le bouton **Editeur** pour voir
comment une chanson particulière est écrite.

## Notes

Une note peut être ajoutée à une chanson en écrivant le nom de la note suivi du
octave. Les notes sont placées après la dernière note ajoutée ou le début d’une
chanson vide. Les notes doivent être séparées par des espaces (nouvelles lignes, espaces, onglets,
etc.).

    c5 d5 e5

Une durée peut être spécifiée en ajoutant une période et le multiplicateur à la fin de
une note. Le multiplicateur de durée par défaut est «1» et la durée de base par défaut est 1 temps.

    c5.2 d5 d5 e5.4

Vous pouvez modifier la durée de base à l’aide de `dt` (temps double),` ht` (demi
time), ou `tt` (temps triple). Ceux-ci prennent effet jusqu'à la fin de la
chanson (ou bloc). Par exemple, nous pouvons facilement écrire une 8e note avec `dt`:

    dt
    c5 d5 c5 d5 c5 d5 e5.2

Vous pouvez utiliser plusieurs commandes `dt` (ou` ht`) pour réduire (ou augmenter) la durée
encore plus.

Vous pouvez reculer la position actuelle en utilisant le caractère `|`. Cela va écrire
un accord en do majeur.

    c5 | d5 | e5

Voici comment vous pouvez écrire deux voix :

    c5 g5 e5.2
    | 
    c4.2 f4.2

Mettre un «|» au début de la chanson (ou du bloc) n’a aucun effet, donc un autre
façon d'écrire ce qui précède pourrait être :

    | c5 g5 e5.2
    | c4.2 f4.2

Une note peut être rendue nette avec `+`, plate avec `--` et naturelle avec `=`. Celles-ci
les modificateurs de note apparaissent après le nom de la note, mais avant l'octave.

    c+5 c-5

## Repos

Vous pouvez insérer un silence en utilisant la commande rest, `r`. Vous pouvez spécifier une durée
multiplicateur en incluant un nombre après la commande `r`. La durée des pauses
fonctionne de la même manière que les notes, leur durée de base est affectée par `dt` et` ht`, et
ils ont une durée de base par défaut de 1 temps.

    c5 r d5.2
    d5 r2 a4

## Blocs

Un bloc est délimité par un `{` et `}`. Les blocs peuvent être imbriqués. Les blocs s'adaptent
comment fonctionnent certaines commandes :

* `|` commande va déplacer la position au début du bloc
* La durée de base (ajustée avec `dt` et` ht`) reviendra à la valeur précédente après

exemple :

    {
      {
        dt
        c5 { dt e5 f5 } d5.2 e5 g5 a5 c6
      }
      |
      { ht g4 f4 }
    }

## Mesurer les mouvements

La commande de mesure, `m`, peut être utilisée pour déplacer la position actuelle vers un
mesure spécifique. Il est courant d’utiliser la commande `m` suivie d’un bloc pour
organiser un morceau :


    m0 {
      | c5 c5 a5 g5
      | g4.4
    }

    m1 {
      | d5 d5 a5 e5
      | f4.4
    }

## Signature clé

Vous pouvez spécifier le nombre de dièses ou de plats à utiliser avec la commande `ks`
suivi d'un nombre positif ou négatif. Les notes que vous écrivez seront automatiquement
être tranchant ou plat comme l'exige la signature de la clé. Vous pouvez utiliser le `=` naturel
modificateur pour supprimer le tranchant ou plat.

    ks-2
    b5 c5 b=5

## Signature temporelle

Définissez la signature temporelle à l’aide de la commande `ts`. Cela définira le temps `3 / 4`.

    ts3/4

    c5 g5 g5

La signature temporelle changera où les lignes de mesure apparaissent et affectera combien
*bat* remplir une mesure entière. La signature temporelle affecte également où
mesurer les mouvements placera la position de la note.

## Commandes de temps

Les commandes de temps changent le multiplicateur de temps actuel jusqu'à la fin du morceau ou
bloquer. Les commandes de temps sont multipliées par rapport au multiplicateur de temps actuel.
vous pouvez répéter les commandes pour empiler l'effet.

Les notes ont une durée en temps allant de 1 à n’importe quel nombre. Commandes de temps
changer le multiplicateur de durée qui affecte la durée réelle de la note.

Lorsqu'une note est écrite sans qu'une durée ne soit spécifiée, sa durée est de 1 temps.
Sinon, n'importe quel nombre entier peut être spécifié comme longueur d'une note.

Par défaut, le multiplicateur de temps est `1`. Dans le temps `4 / 4`, cela signifie chaque temps
la note durera 1 temps, donc une noire.

En utilisant **temps double**, le multiplicateur est divisé par deux, ainsi chaque note écrite de 1 temps
deviendra une huitième note: `(1 * 0.5) = 0.5`, chaque note correspond à un demi-temps.

Voici les commandes de temps:

* `dt` - temps double
* `ht` - temps double
* `tt` - Triple Time


## Accords

La commande `$` peut être utilisée pour spécifier un accord en utilisant un symbole d'accord. La corde
prendra toute la durée du bloc dans lequel il est placé. Les accords ne sont pas
avoir une octave. Les notes de l’accord seront automatiquement générées par un
générateur d'accord s'il est activé.

Ce qui suit est une courte mélodie sur une progression d’accord en sol majeur et en ré mineur.

    {$g c5.2 a5 d5}
    {$dm e5 f5 g5.2}

## Commentaires

Si un `#` apparaît, il sera ignoré, ainsi que le texte qui suivra. Vous pouvez utiliser
ceci pour ajouter des notes à toute personne lisant le code (commentaires aka).