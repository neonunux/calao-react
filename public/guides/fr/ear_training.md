# Entraînement d'oreille

Cette application est livrée avec quelques outils de formation d'oreille :

## Reconnaissance par intervalles avec mélodies

Une stratégie courante pour mémoriser les intervalles consiste à associer leur son à
mélodies de chansons populaires que vous avez reconnues. Les mélodies communes sont
inclus pour tous les 12 intervalles ascendant et descendant.

En cliquant sur *Mélodie suivante*, vous pouvez extraire une mélodie de manière aléatoire. Vous pouvez soit
jouer la racine ou la mélodie entière. Essayez de cliquer sur *Play root*, puis essayez de
chantez le reste pour vous aider à mémoriser les mélodies.

Mode de lecture automatique

Le mode de lecture automatique jouera en continu des intervalles aléatoires, suivis d’un court
pause, suivie de la mélodie. Vous pouvez utiliser ceci pour entendre un intervalle, essayer de
devinez-le, puis confirmez en fonction de la mélodie jouée par la suite. C'est bien
façon de pratiquer sans avoir à utiliser vos mains.

Le mode de lecture automatique comporte quelques options supplémentaires:

* **Racine aléatoire** - Transpose aléatoirement la mélodie à chaque fois qu'une nouvelle est sélectionnée
* **Ordre** - Vous pouvez choisir de jouer l’intervalle de manière mélodique ou harmonique (les deux notes en même temps). Vous pouvez également utiliser reverse pour jouer l’intervalle de manière mélodique, mais dans l’ordre inverse, ou un défi supplémentaire.

## Lecture mélodique

L'ordinateur va générer une courte mélodie. Après l'avoir entendu, vous devriez jouer
avec votre clavier MIDI connecté ou le clavier à l’écran.
Jouez correctement la mélodie et obtenez automatiquement la mélodie suivante.

Le timing des notes n’est pas pris en compte, seuls les terrains que vous jouez sont
vérifié. Vous pouvez ignorer la mélodie en cliquant sur * Next Melody * ou l'entendre à nouveau en
en cliquant sur * Répéter la mélodie *. Vous pouvez aussi déclencher la répétition de la mélodie en utilisant
l'un des curseurs, boutons ou pédales de votre contrôleur MIDI.

Les options suivantes sont disponibles:

* **Notes par mélodie** - Le nombre de notes dans la mélodie générée
* **Mélodie continue** - La mélodie générée suivante est poursuivie à partir de la mélodie générée précédemment. Cela fera que l’intervalle entre la fin de la précédente et le début de la suivante sera généralement assez petit
* **Notes par colonne** - Combien de notes à jouer simultanément
* **Direction** - Quel intervalle de direction dans la mélodie peut aller
* **Échelle** - Limite les notes potentielles à une échelle spécifique