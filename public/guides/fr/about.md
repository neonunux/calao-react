# à propos de Calao

Ce site est un moyen libre et gratuit de pratiquer la 
[lecture à vue](https://fr.wikipedia.org/wiki/Lecture_%C3%A0_vue) en jouant de manière aléatoire
musique générée ou en jouant avec des chansons préprogrammées.
De plus, vous trouverez d'autres outils pour vous aider avec votre oreille et votre musique
la théorie en général.

**Ce projet est 100% gratuit & open-source**, vous pouvez contribuer au développement
sur Gitlab : <https://gitlab.com/neonunux/calao>

## un travail perfectible

Cette application est loin d'être achevée et est encore en développement. Pour
les idées, suggestions ou commentaires merci de [les poster sur notre tracker](https://gitlab.com/neonunux/calao/issues).

## les sons

Cette application a un instrument de piano et métronome intégré. Alternativement, vous pouvez
configurez un périphérique de sortie MIDI pour que tout synthétiseur génère du son
pour vous. Configurez le son en cliquant sur le bouton **MIDI** en haut à droite de
la page.

Si vous utilisez le mode *arcade* avec sortie MIDI, un métronome est joué sur
canal 10. Ce devrait être un kit de batterie GM pour sonner correctement.

## les outils

Il existe de nombreux outils inclus pour mettre en pratique votre musique et vos compétences en lecture à vue:

* **personnel** - Des notes sont générées aléatoirement pour que vous puissiez lire et lire à vue. Cela garantira que vous lisez toujours et ne jouez jamais de mémoire. Dans le mode le plus simple, vous jouez une note à la fois, mais vous pouvez augmenter la complexité en cliquant sur **paramétrer** et en modifiant la façon dont les notes sont générées. Pour pouvoir jouer plus d’une note à la fois, vous devez connecter un contrôleur MIDI !
* **arcade** - Des chansons préprogrammées sont disponibles pour votre lecture. Vous pouvez contrôler la vitesse de lecture et configurer un métronome à l'aide de l'option de sortie MIDI.
* **oreille** - Différents outils pour pratiquer votre oreille, y compris la reconnaissance d'intervalle et la reproduction d'une mélodie.
* **cérébral** - Génère des cartes flash aléatoires pour vous aider à mémoriser ou à identifier rapidement des éléments liés à la théorie musicale

## utilisation d'un clavier MIDI

Bien qu'il soit possible de jouer avec votre souris ou votre clavier d'ordinateur, nous
recommande de connecter votre piano numérique ou votre clavier MIDI à votre ordinateur afin
vous pouvez jouer directement dans le logiciel. Un clavier MIDI est requis pour entrer
plusieurs notes en même temps.

Le support MIDI est uniquement disponible dans Chrome pour le moment. Si vous avez un OTG USB
câble, vous pouvez même connecter votre clavier à votre téléphone ou tablette Android.

# conseils pour la lecture à vue

## pratique régulière

La lecture à vue prend beaucoup de temps à apprendre. Une pratique fréquente
est nécessaire. Votre objectif devrait pouvoir voir les notes et instantanément
savoir où déplacer vos mains. Comme vous devenez plus rapide à la lecture, ajoutez plus
notes simultanées.

## Utiliser de la vraie musique

Cette application ne suffit pas pour apprendre à lire à vue. Entraînez-vous à 
lire de véritable partition de musique en plus d'utiliser cette application. 
Lire des choses que vous avez l'habileté de jouer. La lecture à vue ne consiste 
pas seulement à reconnaître les notes, mais aussi à jouer ce qui est écrit. 
Si vous n'avez pas encore la coordination pour jouer quelque chose, alors
mémorisez cette pièce et trouvez autre chose à lire à vue.

> **conseil de l'auteur** une boutique de partition d'occasion permettra de 
> renouveler les lectures : on y trouvera beaucoup de musiques jamais vues 
> auparavant, de tous les niveaux.

## pratique sans regarder les mains

Regarder ses mains signifie plus de fatigue visuelle, moins de temps pour lire 
les partitions et donc plus d'erreurs. Il faut viser à jouer les notes sans baisser les yeux. Il ne s’agit pas de mémoriser l’emplacement de chaque
notez sur le clavier, mais comprenez plutôt comment vos mains bougeront pour aller
aux notes suivantes à partir des notes déjà jouées.

## s'entraîner à découper et à lire à l'avance

Couper en morceaux signifie lire un bloc de notes à la fois, puis le jouer à partir de votre
Mémoire. La lecture vous donne le temps de comprendre comment bouger vos mains comme
vous jouez

