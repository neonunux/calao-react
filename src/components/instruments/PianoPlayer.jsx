import Soundfont from 'soundfont-player';

let pianoPlayer = null;

// TODO: Provide a way to control when to load the player.
Soundfont.instrument(
  new AudioContext(),
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
  require('!!file-loader!../../soundfonts/MusyngKite/acoustic_grand_piano-mp3'),
).then((player) => {
  pianoPlayer = player;
});

export default function playNote(note) {
  if (pianoPlayer !== null) {
    pianoPlayer.play(note);
  }
}
