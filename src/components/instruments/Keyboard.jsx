import '@/styles/keyboard.scss';

import classNames from 'classnames';
import { noop, over, range } from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';

import { combineRefs } from '@/hooks/helpers';
import { useChange } from '@/hooks/mutations';
import { useMouseDown, useTouch } from '@/hooks/pointers';
import { getNote, getPitch, isAccidental } from './noteHelpers';
import playNote from './PianoPlayer';

function getColor(note) {
  return isAccidental(note) ? 'black' : 'white';
}

export default function Keyboard({
  lowerNote,
  upperNote,
  onKeyDown,
  onKeyUp,
}) {
  const lowerPitch = getPitch(lowerNote);
  const upperPitch = getPitch(upperNote);

  if (lowerPitch >= upperPitch) {
    throw Error('lowerNote must have a lower pitch than upperNote.');
  }

  const keys = range(lowerPitch, upperPitch + 1).map((pitch) => (
    <KeyboardKey
      key={pitch}
      note={getNote(pitch)}
      onDown={over(playNote, onKeyDown)}
      onUp={onKeyUp}
    />
  ));

  return (
    <div className="keyboard">{keys}</div>
  );
}

Keyboard.propTypes = {
  lowerNote: PropTypes.string,
  upperNote: PropTypes.string,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
};

Keyboard.defaultProps = {
  lowerNote: 'C5',
  upperNote: 'B6',
  onKeyDown: noop,
  onKeyUp: noop,
};

function KeyboardKey({
  note,
  onDown,
  onUp,
}) {
  const color = getColor(note);
  const [isMouseDown, mouseDownRef] = useMouseDown();
  const [isTouched, touchRef] = useTouch();
  const isDown = isMouseDown || isTouched;
  const [hasChanged] = useChange(isDown);

  const classes = classNames(
    'keyboard-key',
    { 'keyboard-key--white': color === 'white' },
    { 'keyboard-key--black': color === 'black' },
    { 'keyboard-key--pressed': isDown },
  );

  useEffect(() => {
    if (hasChanged && isDown) onDown(note);
    if (hasChanged && !isDown) onUp(note);
  });

  return (
    <button
      ref={combineRefs(mouseDownRef, touchRef)}
      type="button"
      className={classes}
    >
      {note}
    </button>
  );
}

KeyboardKey.propTypes = {
  note: PropTypes.string.isRequired,
  onDown: PropTypes.func,
  onUp: PropTypes.func,
};

KeyboardKey.defaultProps = {
  onDown: noop,
  onUp: noop,
};
