const OCTAVE_SIZE = 12;

const PITCH_CLASSES = [
  [0, 'C'],
  [2, 'D'],
  [4, 'E'],
  [5, 'F'],
  [7, 'G'],
  [9, 'A'],
  [11, 'B'],
];

const OFFSET_TO_LETTERS = new Map(PITCH_CLASSES);
const LETTERS_TO_OFFSET = new Map(PITCH_CLASSES.map((entry) => entry.reverse()));

export function getNote(pitch, favorSharp = true) {
  const octave = Math.floor(pitch / OCTAVE_SIZE);
  const offset = pitch % OCTAVE_SIZE;
  const toneNotation = !OFFSET_TO_LETTERS.has(offset)
    ? OFFSET_TO_LETTERS.get(offset + (favorSharp ? -1 : 1)) + (favorSharp ? '#' : 'b')
    : OFFSET_TO_LETTERS.get(offset);
  return toneNotation + octave.toString(10);
}

export function getPitch(note) {
  const [, letter, accidentalSymbol, octaveNotation = '0'] = note.match(/^([A-G])(#|b)?(\d+)?$/) || [];
  if (letter === undefined) {
    throw Error(`Invalid or unsupported note notation, got ${note}.`);
  }
  const accidentalOffset = 0
    + (accidentalSymbol === '#' ? 1 : 0)
    + (accidentalSymbol === 'b' ? -1 : 0);
  return (
    OCTAVE_SIZE * parseInt(octaveNotation, 10)
    + LETTERS_TO_OFFSET.get(letter)
    + accidentalOffset
  );
}

export function isAccidental(pitchOrNote) {
  const pitch = (typeof pitchOrNote === 'number')
    ? pitchOrNote
    : getPitch(pitchOrNote);
  return !OFFSET_TO_LETTERS.has(pitch % 12);
}
