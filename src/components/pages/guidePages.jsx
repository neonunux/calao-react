import '../../scss/guidePage.scss';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { NavLink } from 'react-router-dom';
import types from 'prop-types';
import { setTitle, getLocale } from '../../uncategorized/globals';

export default class GuidePage extends React.Component {
  static propTypes = {
    title: types.string.isRequired,
    pageSource: types.string.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { title, pageSource } = this.props;
    setTitle(title);

    const request = new XMLHttpRequest();
    request.open('GET', `/guides/${getLocale()}/${pageSource}.md`);
    request.send();

    request.onload = () => {
      const contents = request.responseText;
      this.setState({
        contents,
      });
    };

    this.request = request;
  }

  componentWillUnmount() {
    if (this.request) {
      this.request.abort();
      delete this.request;
    }
  }

  renderContents() {
    const { contents } = this.state;
    if (contents) {
      return (
        <div>
          <section className="page_container">
            <ReactMarkdown
              source={this.state.contents}
              escapeHtml={false}
            />
          </section>
        </div>
      );
    }
    return <div className="loading_message">Loading...</div>;
  }

  render() {
    const link = (url, label) => (
      <NavLink activeClassName="active" to={url}>
        {label}
      </NavLink>
    );


    return (
      <main className="guide_page">
        <section className="page_navigation">
          <section>
            <div className="nav_header">Overview</div>
            <ul>
              <li>{link('/about', 'About')}</li>
              <li>{link('/guide/generators', 'Generators')}</li>
              <li>{link('/guide/chords', 'Chords')}</li>
              <li>{link('/guide/ear-training', 'Ear Training')}</li>
            </ul>
          </section>
          <section>
            <div className="nav_header">Play Along</div>
            <ul>
              <li>{link('/guide/lml', 'LML')}</li>
            </ul>
          </section>
        </section>
        {this.renderContents()}
      </main>
    );
  }
}
