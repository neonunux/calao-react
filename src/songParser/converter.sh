#!/bin/bash

# amd, commonjs, globals, umd
# FORMAT="umd"
# FORMAT="globals"
FORMAT="globals"

cat song_parser_peg.pegjs | ../../node_modules/.bin/pegjs --format $FORMAT \
  > index.js
# cat song_parser_peg.pegjs | ../../node_modules/.bin/pegjs --format $FORMAT \
#  | sed -e 's/^define(/define("peg", /' \
#  > index.js
# cat song_parser_peg.pegjs | ../../node_modules/.bin/pegjs --format $FORMAT \
# --export-var peg | sed -e 's/^define(/define("st\/song_parser_peg", /' \
#  > index.js
