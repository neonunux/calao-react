// eslint-disable-next-line import/prefer-default-export
export function combineRefs(...refs) {
  // eslint-disable-next-line no-param-reassign
  return (node) => refs.forEach((ref) => { ref.current = node; });
}
