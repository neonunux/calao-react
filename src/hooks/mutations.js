import { useRef } from 'react';

export function usePrevious(value) {
  const ref = useRef();
  const previousValue = ref.current;
  ref.current = value;
  return previousValue;
}

export function useChange(value) {
  const previousValue = usePrevious(value);
  return [value !== previousValue, previousValue];
}
