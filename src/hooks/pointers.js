import { useEffect, useRef, useState } from 'react';

function isTouchOnElement(touch, element) {
  return element.contains(document.elementFromPoint(touch.clientX, touch.clientY));
}

export function useMouseDown() {
  const elementRef = useRef();
  const [isMouseDown, setIsMouseDown] = useState(false);

  useEffect(() => {
    const element = elementRef.current;

    const mouseDownListener = () => { setIsMouseDown(true); };
    const mouseUpOrOutListener = () => { setIsMouseDown(false); };

    element.addEventListener('mousedown', mouseDownListener);
    element.addEventListener('mouseup', mouseUpOrOutListener);
    element.addEventListener('mouseout', mouseUpOrOutListener);

    return () => {
      element.removeEventListener('mousedown', mouseDownListener);
      element.removeEventListener('mouseup', mouseUpOrOutListener);
      element.removeEventListener('mouseout', mouseUpOrOutListener);
    };
  });

  return [isMouseDown, elementRef];
}

export function useTouch() {
  const elementRef = useRef();
  const touchesRef = useRef(new Set());
  const [isTouched, setIsTouched] = useState(false);

  useEffect(() => {
    const element = elementRef.current;
    const touches = touchesRef.current;

    const updatesIsTouched = (fn) => (...args) => {
      fn(...args);
      setIsTouched(touches.size > 0);
    };

    const touchStartListener = updatesIsTouched((event) => {
      if (event.target === element) {
        event.preventDefault();
      }
      Array.from(event.changedTouches)
        .filter((touch) => isTouchOnElement(touch, element))
        .forEach((touch) => touches.add(touch.identifier));
    });

    const touchMoveListener = updatesIsTouched((event) => {
      Array.from(event.changedTouches)
        .filter((touch) => !isTouchOnElement(touch, element))
        .forEach((touch) => touches.delete(touch.identifier));
    });

    const touchEndListener = updatesIsTouched((event) => {
      Array.from(event.changedTouches)
        .forEach((touch) => touches.delete(touch.identifier));
    });

    document.addEventListener('touchstart', touchStartListener, { passive: false });
    document.addEventListener('touchmove', touchMoveListener);
    document.addEventListener('touchend', touchEndListener);

    return () => {
      document.removeEventListener('touchstart', touchStartListener, { passive: false });
      document.removeEventListener('touchmove', touchMoveListener);
      document.removeEventListener('touchend', touchEndListener);
    };
  });

  return [isTouched, elementRef];
}
