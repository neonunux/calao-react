import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector'; // <= new
import en from './en.json';
import fr from './fr.json';

const resources = {
  en,
  fr,
};

i18n
  .use(LanguageDetector)
  .init({
    resources,
    // lng: getLocale(),
    keySeparator: true,
  });
