import React from 'react';
import ReactDOM from 'react-dom';

import Keyboard from './components/instruments/Keyboard';

ReactDOM.render(
  <Keyboard lowerNote="C6" upperNote="G6" />,
  document.getElementById('root'),
);
