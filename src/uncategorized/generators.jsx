import MersenneTwister from 'mersennetwister';
import {
  parseNote, noteName, MajorScale, Chord,
} from './music';

import { shuffled } from './util';

// takes generator object from data
export function generatorDefaultSettings(generator, staff) {
  const out = {};

  if (!generator.inputs) {
    return out;
  }

  const defaultValue = (input) => {
    if ('default' in input) {
      return input.default;
    }

    switch (input.type) {
      case 'select':
        return input.values[0].name;
      case 'range':
        return input.min;
      case 'noteRange':
        if (staff && input.name === 'noteRange') {
          return [parseNote(staff.range[0]), parseNote(staff.range[1])];
        }
        return [input.min, input.max];

      default: break;
    }
  };

  for (const input of generator.inputs) {
    out[input.name] = defaultValue(input);
  }

  return out;
}

// strip any values that don't make sense
export function fixGeneratorSettings(generator, settings) {
  const out = {};

  if (!generator.inputs) {
    return out;
  }

  for (const input of generator.inputs) {
    let currentValue = settings[input.name];
    if (currentValue != null) {
      switch (input.type) {
        case 'select': {
          const found = input.values.filter((v) => v.name === currentValue);
          if (!found) {
            currentValue = null;
          }
          break;
        }
        case 'range': {
          if (typeof currentValue === 'number') {
            currentValue = Math.min(
              input.max,
              Math.max(input.min, +currentValue),
            );
          } else {
            currentValue = null;
          }
          break;
        }
        case 'bool': {
          if (typeof currentValue !== 'boolean') {
            currentValue = null;
          }
          break;
        }
        default: break;
      }

      if (currentValue === null) {
        console.warn(`Truncating generator input: ${input.name}`);
        continue;
      }

      out[input.name] = currentValue;
    }
  }

  return out;
}

export function testRandomNotes() {
  const scale = new MajorScale('C');
  // let notes = scale.getLooseRange("A4", "C7")
  const notes = scale.getLooseRange('C3', 'C7');

  const r = new RandomNotes(notes, {});

  let totalCount = 0;
  const counts = {};
  for (const note of notes) {
    counts[note] = 0;
  }

  for (let i = 0; i < 10000; i++) {
    for (const group of r.handGroups()) {
      for (const n of group) {
        counts[n] += 1;
      }
    }
    totalCount += 1;
  }

  console.log('total', totalCount, counts);
  const ratios = {};

  for (const note of notes) {
    ratios[note] = (counts[note] / totalCount) * 100;
  }

  console.log('ratios', ratios);
}

export function testSkewRand(iterations = 1) {
  const r = new RandomNotes([], {});
  const counts = {};

  // let totalCount = 0
  for (let i = 0; i < 10000; i++) {
    const k = r.skewRand(5, iterations);
    counts[k] = (counts[k] || 0) + 1;
    // totalCount++
  }

  console.log(counts);
}

export class Generator {
  constructor(opts = {}) {
    this.smoothness = opts.smoothness || 0;
  }

  averagePitch(notes) {
    if (notes.length === 0) {
      throw new Error('trying to find average of empty note list');
    }

    const pitches = notes.map(parseNote);
    return pitches.reduce((a, b) => a + b, 0) / pitches.length;
  }

  _nextNote() {
    throw new Error('missing _nextNote implementation');
  }

  nextNote() {
    return this.nextNoteSmooth(this.smoothness + 1, this._nextNote.bind(this));
  }

  // sort by minimizing min pitch difference
  sortedCandidatesIndividual(iterations = 1, nextNote) {
    if (iterations === 1) {
      return nextNote();
    }

    if (!this.lastNotes) {
      this.lastNotes = nextNote();
      return this.lastNotes;
    }

    const pitches = this.lastNotes.map(parseNote);

    const candidates = [];
    for (let i = 0; i < iterations; i++) {
      const c = nextNote();
      let score = 0;

      for (const n of c) {
        const scores = pitches.map((p) => Math.abs(p - parseNote(n)));
        score += Math.min(...scores);
      }

      candidates.push([score, c]);
    }

    candidates.sort(([a], [b]) => a - b);
    return candidates;
  }

  // sorts by minimizing average pitch
  sortedCandidatesAverage(iterations, nextNote) {
    const target = this.averagePitch(this.lastNotes);

    const candidates = [];
    for (let i = 0; i < iterations; i++) {
      const c = nextNote();
      const avg = this.averagePitch(c);

      candidates.push([Math.abs(avg - target), c]);
    }

    candidates.sort(([a], [b]) => a - b);
    return candidates;
  }

  nextNoteSmooth(iterations = 1, nextNote) {
    // not smoothing, don't care
    if (iterations === 1) {
      return nextNote();
    }

    if (!this.lastNotes) {
      this.lastNotes = nextNote();
      return this.lastNotes;
    }

    const candidates = this.sortedCandidatesIndividual(iterations, nextNote);
    let out = candidates[0][1]; // abandon case

    for (const [diff, notes] of candidates) {
      // no repeats
      if (
        diff === 0
        && notes.sort().join('-') === this.lastNotes.sort().join('-')
      ) {
        continue;
      }

      out = notes;
      break;
    }

    this.lastNotes = out;
    return out;
  }
}

export class RandomNotes extends Generator {
  handSize = 11

  constructor(notes, opts = {}) {
    super(opts);
    this.generator = new MersenneTwister();
    this.notes = notes;
    this.notesPerColumn = opts.notes || 1;
    this.scale = opts.scale;
    this.hands = opts.hands || 2;
  }

  // divide up items into n groups, pick a item from each group
  // items: list of items
  // n: number of groups (and items selected)
  pickNDist(items, n) {
    if (!items.length) {
      return [];
    }

    if (n === 0) {
      return [];
    }

    if (n === 1) {
      return [items[this.generator.int() % items.length]];
    }

    const groups = [];

    for (let k = 0; k < items.length; k++) {
      const group = Math.min(n - 1, Math.floor((k / (items.length - 1)) * n));

      if (!groups[group]) {
        groups[group] = [];
      }

      groups[group].push(items[k]);
    }

    return groups.map((g) => g[this.generator.int() % g.length]);
  }

  getNotesForHand(pitches, left) {
    const start = pitches[0] + left;
    return pitches
      .map((p) => p - start)
      .filter((p) => p >= 0 && p < this.handSize)
      .map((p) => p + start); // put it back
  }

  // generate random number [0,n[ with skew towards 0 based on normal dist
  // iterations controls how normal the normal dist is, 1 is flat dist
  skewRand(n, iterations = 1) {
    let r = 0;

    for (let i = 0; i < iterations; i++) {
      r += this.generator.random();
    }

    // from 0 to 1 with bias towards 0
    r = Math.abs((r / iterations - 0.5) * 2);
    return Math.floor(n * r);
  }

  handGroups(notes = this.notes) {
    const pitches = notes.map(parseNote);
    pitches.sort((a, b) => a - b);

    const range = pitches[pitches.length - 1] - pitches[0];

    // rake a random hand if we only need one
    if (this.hands === 1) {
      const rootRange = range - this.handSize;
      return [
        this.getNotesForHand(pitches, this.generator.int() % rootRange).map(
          noteName,
        ),
      ];
    }

    // how much space between hands if hands are at ends
    const handSpace = range - 2 * this.handSize + 1;
    const firstHandMovement = handSpace > 0 ? this.skewRand(handSpace, 2) : 0;
    const remainingSpace = handSpace - firstHandMovement;
    const secondHandMovement = remainingSpace > 0 ? this.skewRand(remainingSpace, 2) : 0;

    const rightHandStart = range - this.handSize + 1;
    const moveLeftFirst = this.generator.int() % 2 === 0;

    let leftHand; let
      rightHand;

    if (moveLeftFirst) {
      leftHand = this.getNotesForHand(pitches, firstHandMovement);
      rightHand = this.getNotesForHand(
        pitches,
        rightHandStart - secondHandMovement,
      );
    } else {
      leftHand = this.getNotesForHand(pitches, secondHandMovement);
      rightHand = this.getNotesForHand(
        pitches,
        rightHandStart - firstHandMovement,
      );
    }

    // resolve overlaps
    if (leftHand[leftHand.leftHand - 1] > rightHand[0]) {
      console.warn('fixing overlap');
      const mid = leftHand[leftHand.leftHand - 1] + rightHand[0] / 2;
      leftHand = leftHand.filter((n) => n <= mid);
      rightHand = rightHand.filter((n) => n > mid);
    }

    return [leftHand.map(noteName), rightHand.map(noteName)];
  }

  notesInRandomChord() {
    const degree = 1 + (this.generator.int() % this.scale.steps.length);
    const steps = this.scale.buildChordSteps(degree, 3); // seven chords
    const chord = new Chord(this.scale.degreeToName(degree), steps);
    this.lastChord = chord;
    return this.notes.filter((n) => chord.containsNote(n));
  }

  nextNoteWithoutAnnotation() {
    this.lastChord = null;
    const notes = this.scale ? this.notesInRandomChord() : this.notes;

    if (this.notesPerColumn < (this.hands === 1 ? 2 : 3)) {
      // skip the hand stuff since it messes with the distribution
      return this.pickNDist(notes, this.notesPerColumn);
    }

    const hands = this.handGroups(notes);

    if (hands.length === 1) {
      return this.pickNDist(hands[0], this.notesPerColumn);
    }

    // take some notes from each hand group
    let notesForLeft = Math.floor(this.notesPerColumn / 2);
    let notesForRight = Math.floor(this.notesPerColumn / 2);

    // odd amount, randomly assign last note
    if (this.notesPerColumn % 2 === 1) {
      if (this.generator.int() % 2 === 0) {
        notesForLeft += 1;
      } else {
        notesForRight += 1;
      }
    }

    return this.pickNDist(hands[0], notesForLeft).concat(
      this.pickNDist(hands[1], notesForRight),
    );
  }

  _nextNote() {
    const out = this.nextNoteWithoutAnnotation();

    // // how to annotate chords:
    // if (this.lastChord) {
    //   out.annotation = this.lastChord.root + this.lastChord.chordShapeName()
    // }

    return out;
  }
}

// for debugging staves
export class SweepRangeNotes {
  constructor(notes) {
    this.notes = notes;
    this.i = 0;
    this.ascending = true;
  }

  nextNote() {
    if (this.i < 0) {
      this.i = 1;
      this.ascending = !this.ascending;
    }

    if (this.i >= this.notes.length) {
      this.i = this.notes.length - 2;
      this.ascending = !this.ascending;
    }

    if (this.ascending) {
      return this.notes[this.i++ % this.notes.length];
    }
    return this.notes[this.i-- % this.notes.length];
  }
}

export class MiniSteps {
  constructor(notes) {
    this.notes = notes;
    this.generator = new MersenneTwister();
  }

  nextStep() {
    return {
      position: this.generator.int() % this.notes.length,
      remaining: 2 + (this.generator.int() % 2),
      direction: this.generator.int() % 2 === 0 ? 1 : -1,
    };
  }

  nextNote() {
    if (!this.currentStep || this.currentStep.remaining === 0) {
      this.currentStep = this.nextStep();
    }

    const position = this.currentStep.position + this.notes.length;
    this.currentStep.position += this.currentStep.direction;
    this.currentStep.remaining -= 1;

    return this.notes[position % this.notes.length];
  }
}

export class ShapeGenerator extends Generator {
  constructor(opts) {
    super(opts);
    this.generator = new MersenneTwister();
  }

  _nextNote() {
    const shape = this.shapes[this.generator.int() % this.shapes.length];
    const shapeMax = Math.max(...shape);

    if (shapeMax > this.notes.length) {
      throw Error('shape too big for available notes');
    }

    const bass = this.generator.int() % (this.notes.length - shapeMax);

    return shape.map((offset) => this.notes[(bass + offset) % this.notes.length]);
  }

  // get the shape and all the inversions for it
  inversions(shape) {
    shape = [...shape];
    shape.sort((a, b) => a - b);

    const out = [shape];
    let count = shape.length - 1;

    while (count > 0) {
      const dupe = [...out[out.length - 1]];
      dupe.push(dupe.shift() + 7);
      dupe.sort((a, b) => a - b);

      while (dupe[0] > 0) {
        for (const i in dupe) {
          dupe[i] -= 1;
        }
      }
      out.push(dupe);
      count--;
    }

    return out;
  }
}

export class TriadNotes extends ShapeGenerator {
  constructor(notes, opts) {
    super(opts);
    this.notes = notes;
    this.shapes = this.inversions([0, 2, 4]);
  }
}

export class SevenOpenNotes extends ShapeGenerator {
  constructor(notes, opts) {
    super(opts);

    this.notes = notes;
    // some random inversions spaced apart
    this.shapes = [
      // root on bottom
      [0, 4, 9, 13],
      [0, 6, 9, 11],

      // third on bottom
      [2 - 2, 6 - 2, 11 - 2, 14 - 2],
      [2 - 2, 7 - 2, 11 - 2, 13 - 2],

      // fifth on bottom
      [4 - 4, 6 - 4, 9 - 4, 14 - 4],
      [4 - 4, 7 - 4, 9 - 4, 13 - 4],
    ];
  }
}

export class ProgressionGenerator extends Generator {
  constructor(scale, range, progression, opts) {
    super(opts);
    this.position = 0;
    this.progression = progression;
    this.generator = new MersenneTwister();
    this.scale = scale;
    this.range = range;

    // calculate all the roots we can use to build chords on top of
    const roots = scale.getLooseRange(...range);
    this.rootsByDegree = {};

    for (const r of roots) {
      const degree = scale.getDegree(r);
      this.rootsByDegree[degree] = this.rootsByDegree[degree] || [];
      this.rootsByDegree[degree].push(r);
    }
  }

  _nextNote() {
    const [degree, shape] = this.progression[
      this.position % this.progression.length
    ];
    this.position += 1;

    const name = this.scale.degreeToName(degree);
    const chord = new Chord(name, shape);
    const notes = this.scale
      .getLooseRange(...this.range)
      .filter((n) => chord.containsNote(n));

    const notesPerChord = 4;
    const starts = notes.length - notesPerChord;

    const p = this.generator.int() % starts;

    return notes.slice(p, p + notesPerChord);
  }

  nextNoteOld() {
    const [degree, chord] = this.progression[
      this.position % this.progression.length
    ];
    const availableRoots = this.rootsByDegree[degree];
    this.position += 1;

    if (!availableRoots) {
      throw new Error("chord doesn't fit in scale range");
    }

    // console.log("availalbe roots", availableRoots)
    // console.log(chord)

    return Chord.notes(
      availableRoots[this.generator.int() % availableRoots.length],
      chord,
    );
  }
}

// a generator that generates series of notes from positions
export class PositionGenerator extends Generator {
  constructor(notes, opts) {
    super(opts);
    this.notes = notes;
    this.generator = new MersenneTwister();
  }

  getFingerSet() {
    // choose a finger
    const offset = this.generator.int() % (this.notes.length - 5);
    return [0]
      .concat(shuffled([1, 2, 3, 4], this.generator))
      .map((i) => this.notes[offset + i]);
  }

  nextNote() {
    let first = false;

    if (!this.fingerSet || !this.fingerSet.length) {
      this.fingerSet = this.getFingerSet();
      first = true;
    }

    const out = [this.fingerSet.shift()];
    if (first) {
      out.annotation = '1';
    }

    return out;
  }
}

export class IntervalGenerator extends Generator {
  constructor(notes, opts) {
    super(opts);
    this.notes = notes;
    console.log(notes);

    let seed = new Date().getTime();
    seed = 1509875466444;
    console.log('interval with seed', seed);
    this.generator = new MersenneTwister(seed);
    this.opts = opts;
  }

  nextNote() {
    // hard code interval for testing
    const inputIntervals = this.opts.intervals || {};
    const intervals = {};
    for (const key of Object.keys(inputIntervals)) {
      if (key.match(/\d+/) && inputIntervals[key]) {
        intervals[key] = true;
      }
    }

    if (Object.keys(intervals).length === 0) {
      return 'C5';
    }

    if (this.currentNote != null) {
      const intervalArray = Object.keys(intervals).map((i) => +i - 1);
      const interval = intervalArray[this.generator.int() % intervalArray.length];
      let sign = this.generator.int() % 2 === 0 ? -1 : 1;
      const nextNote = this.currentNote + sign * interval;

      if (nextNote < 0 || nextNote >= this.notes.length) {
        sign = -sign;
      }

      this.currentNote = this.currentNote + sign * interval;
    } else {
      this.currentNote = this.generator.int() % this.notes.length;
    }

    return this.notes[this.currentNote];
  }
}
