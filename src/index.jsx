import './scss/main.scss'

import App from './components/uncategorized/app'
import ReactDOM from 'react-dom'
import React from 'react'
import { N } from './uncategorized/globals'
import './i18n'

N.enable_presets = false
N.init = init

export function init(session) {
  N.session = session || {}
  ReactDOM.render(<App />, document.getElementById('root'))
  // installServiceWorker(session.cacheBuster)
}

export function testPage(session) {
  N.session = session || {}
  ReactDOM.render(
    <App layout={App.BlankLayout} />,
    document.getElementById('root'),
  )
}

export function installServiceWorker(timestamp) {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register(`/sw.js?${timestamp}`).then(
      function (registration) {
        console.log('Service worker registered', registration.scope)
      },
      function (err) {
        console.error('Service worker failed to register', err)
      },
    )
  }
}

N.init()
